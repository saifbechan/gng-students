import React from 'react';
import PropTypes from 'prop-types';
import Button from 'react-bootstrap/Button';

const StudentListing = ({ data, handleDelete }) => (
  <tr>
    <td>{data.name}</td>
    <td>{data.dateofbirth}</td>
    <td>
      <Button
        onClick={handleDelete}
        type="submit"
        value={data.id}
      >
        Delete
      </Button>
    </td>
  </tr>
);

StudentListing.propTypes = {
  data: PropTypes.shape({
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    dateofbirth: PropTypes.number.isRequired,
  }),
  handleDelete: PropTypes.func.isRequired,
};

StudentListing.defaultProps = {
  data: {},
};

export default StudentListing;
