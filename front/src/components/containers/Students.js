import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import StudentListing from '../presentation/StudentListing';
import { fetchStudents, deleteStudent } from '../../actions/actions';

class Students extends Component {
  componentDidMount() {
    this.props.dispatch(fetchStudents());
  }

  handleDelete = (e) => {
    e.preventDefault();
    e.stopPropagation();

    this.props.dispatch(deleteStudent(e.target.value));
  };

  render() {
    if (this.props.students.length === 0) {
      return (
        <div>
          There are no students enrolled at the moment.
        </div>
      );
    }

    return (
      <table width="100%">
        <tbody>
          <tr>
            <td>Name</td>
            <td>Age</td>
            <td>Actions</td>
          </tr>
          {this.props.students.map((student, i) => (
            <StudentListing
              // eslint-disable-next-line react/no-array-index-key
              key={i}
              data={student}
              handleDelete={this.handleDelete}
            />
          ))}
        </tbody>
      </table>
    );
  }
}

const mapStateToProps = (state) => ({
  students: state.students.students,
});

Students.propTypes = {
  dispatch: PropTypes.func.isRequired,
  students: PropTypes.arrayOf(PropTypes.shape()),
};

Students.defaultProps = {
  students: [],
};

export default connect(mapStateToProps)(Students);
