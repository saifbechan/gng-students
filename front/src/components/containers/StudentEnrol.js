import React, { useState } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import Col from 'react-bootstrap/Col';
import { enrolStudent } from '../../actions/actions';

const StudentEnrol = ({ dispatch, history }) => {
  const [values, setValues] = useState({
    name: '',
    dateofbirth: 0,
    validated: false,
  });

  const handleInputChange = (e) => {
    const { name, value } = e.target;
    setValues({ ...values, [name]: value });
  };

  const handleSubmit = (e) => {
    const form = e.currentTarget;

    setValues({ ...values, validated: true });

    e.preventDefault();
    e.stopPropagation();

    if (form.checkValidity() === false) {
      return;
    }

    dispatch(enrolStudent(values));
    history.push('/');
  };

  return (
    <Form noValidate validated={values.validated} onSubmit={handleSubmit}>
      <Form.Group as={Col} md="4" controlId="validationCustomName">
        <Form.Label>Name</Form.Label>
        <Form.Control
          required
          name="name"
          type="text"
          placeholder="Enter your name"
          onChange={handleInputChange}
          value={values.name}
        />
        <Form.Control.Feedback type="invalid">
          Please fill in your name.
        </Form.Control.Feedback>
      </Form.Group>
      <Form.Group as={Col} md="4" controlId="validationCustomDateOfBirth">
        <Form.Label>Date of birth</Form.Label>
        <Form.Control
          required
          name="dateofbirth"
          type="date"
          max={new Date().toISOString().split('T')[0]}
          placeholder="Enter a valid date of birth"
          onChange={handleInputChange}
          value={values.dateofbirth}
        />
        <Form.Control.Feedback type="invalid">
          Please enter your date of birth.
        </Form.Control.Feedback>
      </Form.Group>
      <Form.Group as={Col} md="4">
        <Button type="submit">Enrol Student</Button>
      </Form.Group>
    </Form>
  );
};

StudentEnrol.propTypes = {
  dispatch: PropTypes.func.isRequired,
  history: PropTypes.shape().isRequired,
};

export default withRouter(connect(() => ({}))(StudentEnrol));
