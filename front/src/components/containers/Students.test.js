import React from 'react';
import { shallow } from 'enzyme';
import Students from './Students';

it('shows a message when there are no students found', () => {
  const students = shallow(<Students
    store={{
      getState: () => ({ students: [] }),
      dispatch: () => { },
    }}
  />);
  expect(students.html()).toMatchSnapshot();
});

it('shows students when there are some', () => {
  const students = shallow(<Students
    store={{
      getState: () => ({
        students: {
          students: [{
            id: '1242',
            name: 'My Name',
            dateofbirth: 12345,
          }],
        },
      }),
      dispatch: () => { },
      subscribe: jest.fn(),
    }}
  />);
  expect(students.html()).toMatchSnapshot();
});
