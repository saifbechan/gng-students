import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

const Layout = ({ children }) => (
  <Container>
    <Row>
      <Col xs lg="2">
        <ul>
          <li><Link to="/">Home</Link></li>
          <li><Link to="/enrol">Enrol</Link></li>
        </ul>
      </Col>
      <Col>
        <h1>GNG Students board</h1>
        <div>
          {children}
        </div>
      </Col>
    </Row>
  </Container>
);

Layout.propTypes = {
  children: PropTypes.arrayOf(PropTypes.shape()).isRequired,
};

export default Layout;
