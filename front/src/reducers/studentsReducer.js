import constants from '../constants/actionTypes';

const initialState = {
  students: [],
  nstudentsLoading: true,
};

export default (state = initialState, action) => {
  const updated = { ...state };

  switch (action.type) {
    case constants.STUDENTS_RECEIVED:
      updated.students = action.students;
      return updated;

    default:
      return state;
  }
};
