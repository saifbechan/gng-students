import React from 'react';
import { Route, BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import store from './stores/store';
import Layout from './components/layouts/Layout';
import Home from './components/layouts/Home';
import StudentEnrol from './components/containers/StudentEnrol';

class App extends React.PureComponent {
  render() {
    return (
      <Provider store={store}>
        <BrowserRouter>
          <Layout>
            <Route exact path="/" component={Home} />
            <Route path="/enrol" component={StudentEnrol} />
          </Layout>
        </BrowserRouter>
      </Provider>
    );
  }
}

export default App;
