import actionTypes from '../constants/actionTypes';

export function studentsReceived(students) {
  return {
    type: actionTypes.STUDENTS_RECEIVED,
    students,
  };
}

export function fetchStudents() {
  return (dispatch) => fetch('/students')
    .then((response) => response.json())
    .then((data) => dispatch(studentsReceived(data.data)))
    // eslint-disable-next-line no-console
    .catch((e) => console.log(e));
}

export function enrolStudent(data) {
  // eslint-disable-next-line no-unused-vars
  return (dispatch) => fetch('/students', {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(data),
  })
    // eslint-disable-next-line no-console
    .catch((e) => console.log(e));
}

export function deleteStudent(data) {
  // eslint-disable-next-line no-unused-vars
  return (dispatch) => fetch('/students', {
    method: 'DELETE',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(data),
  })
    // eslint-disable-next-line no-console
    .catch((e) => console.log(e));
}
