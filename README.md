Global Nomad Group Students
==
This is a small application showcasing basic React - NodeJS skills. A lot of stuff is stripped out and can be discussed during the interview.

Development
--
Make sure the following applocations are insalled.
 - NodeJS
 - Yarn
 - Nodemon

If everything is installed we can set up our environments and start coding.

Frontend
 - cd front
 - yarn install
 - yarn start

Backend
 - cd back
 - yarn install
 - nodemon app.js

Discussion points
--
 - docker environments
 - CI/CD pipelines
 - functional test
 - database interaction
 - security & authentication
 - Apollo GraphQL
