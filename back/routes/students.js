const express = require('express');

const router = express.Router();

const fakeStudents = [{
  id: '123-123',
  name: 'User One',
  dateofbirth: 431308800,
}, {
  id: '123-234',
  name: 'User Two',
  dateofbirth: 431308800,
}];

router.get('/', (req, res) => {
  res.status(200).send({
    data: fakeStudents,
  });
});

router.post('/', (req, res) => {
  // TODO persist student to the database
  // Data needs to be validated again
  res.json({
    success: 1,
    data: {},
  });
});

router.delete('/', (req, res) => {
  // TODO remove the user from the database
  // Data needs to be validated again
  res.json({
    success: 1,
    data: {},
  });
});

module.exports = router;
