const express = require('express');
const routes = require('./routes/index');
const studentsRoutes = require('./routes/students');

const app = express();
const PORT = process.env.PORT || 5000;

app.use('/', routes);

app.use('/students', studentsRoutes);

app.listen(PORT, () => {
  console.log(`Listening on port ${PORT}`);
});
